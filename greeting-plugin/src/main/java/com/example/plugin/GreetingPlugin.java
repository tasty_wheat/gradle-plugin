package com.example.plugin;

import org.gradle.api.Action;
import org.gradle.api.DefaultTask;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.Task;
import org.gradle.api.tasks.TaskAction;

public class GreetingPlugin implements Plugin<Project> {
    @Override
    public void apply(Project project) {
        // project.getTasks().register("greet", task -> {
            // task.doLast(s -> System.out.println("Hello from plugin 'com.example.plugin.greeting'"));
        // });
        project.getTasks().register("greet", GreetingTask.class);
        // project.getTasks().register("greet", (Action<Task>) new GreetingAction());
        System.out.println("Hello World!");
    }

    public class GreetingAction implements Action<Task> {
        @Override
        public void execute(Task task) {            
            task.doLast(t -> System.out.println("Hello from plugin 'com.example.plugin.greeting'"));
        }
    }

    public class GreetingTask extends DefaultTask {
        @TaskAction
        public void greet() {
            doLast(task -> System.out.println("Hello from plugin 'com.example.plugin.greeting'"));
        }
    }
}
