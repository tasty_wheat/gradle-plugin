package com.example.plugin;

import org.gradle.testfixtures.ProjectBuilder;
import org.gradle.api.Project;
import org.gradle.api.tasks.TaskContainer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;

import static org.mockito.Mockito.*;

import com.example.plugin.GreetingPlugin.GreetingTask;

import static org.junit.jupiter.api.Assertions.assertNotNull;


public class GreetingPluginTest {

    @BeforeEach
    public void setup() {

    }

    @Test
    @Disabled
    public void pluginRegistersATask() {
        // Create a test project and apply the plugin
        Project project = ProjectBuilder.builder().build();
        project.getPlugins().apply("com.example.plugin.greeting");

        // Verify the result
        assertNotNull(project.getTasks().findByName("greet"));
    }

    @Test
    @DisplayName("Plugin should create new task")
    public void unitEst() throws ClassNotFoundException {
        final GreetingPlugin plugin = new GreetingPlugin();

        // Setup mocking
        Project mockProject = mock(Project.class);
        TaskContainer mockContainer = mock(TaskContainer.class);
        doReturn(mockContainer).when(mockProject).getTasks();

        plugin.apply(mockProject);

        verify(mockContainer).register("greet", GreetingTask.class);
    }
}
